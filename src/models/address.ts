
export class Address {
    address: string = null;
    city: string = null;
    state: string = null;
    country: string = null;
    zipcode: string = null;

    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (this[key] instanceof Date) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }

            }
        });
    }
}