import { MetadataHelper } from "@neb-sports/mysamay-common-utils";
import { Address } from './address';

var dateFields = [
    "createdTime",
    "updatedTime"
]
export class RunningGroup {

    _id: string = null;
    groupName: string = null;
    externalGroupId: string = null;
    description: string = null;
    email: string = null;
    mobile: string = null;
    address: Address = null;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: Partial<RunningGroup>) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(dateFields.find(dateField => dateField === key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Groups_RunningGroup";
}